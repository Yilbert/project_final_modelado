def convert_month(month: str):
    if month == 'enero':
        return 1
    elif month == 'febrero':
        return 2
    elif month == 'marzo':
        return 3
    elif month == 'abril':
        return 4
    elif month == 'mayo':
        return 5
    elif month == 'junio':
        return 6
    elif month == 'julio':
        return 7
    elif month == 'agosto':
        return 8
    elif month == 'septiembre':
        return 9
    elif month == 'octubre':
        return 10
    elif month == 'noviembre':
        return 11
    elif month == 'diciembre':
        return 12
    else:
        return 0