from ast import Try
import numpy as np
import pandas as pd
from api.models import *
from datetime import datetime
from .convert_month import convert_month


class SaveData:
    def __init__(self, data):
        self.data = data

    def save_condicion_meteor(self):
        for index in self.data.index:
            try:
                CondicionMeteor.objects.get(condicion=self.data['condic_meteor'][index])
            except CondicionMeteor.DoesNotExist:
                condicion_meteor = CondicionMeteor(
                    condicion=str(self.data['condic_meteor'][index])
                )
            condicion_meteor.save()
        print(self.data['condic_meteor'])

    def save_dia_semana_acc(self):
        for index in self.data.index:
            try:
                DiaSemanaAcc.objects.get(dia_semana=self.data['dia_semana_acc'][index])
            except DiaSemanaAcc.DoesNotExist:
                dia_semana_acc = DiaSemanaAcc(
                    dia_semana=str(self.data['dia_semana_acc'][index])
                )
            dia_semana_acc.save()
        print(self.data['dia_semana_acc'])

    def save_estado_superficie(self):
        for index in self.data.index:
            try:
                EstadoSuperficie.objects.get(superficie=self.data['estado_super'][index])
            except EstadoSuperficie.DoesNotExist:
                estado_superficie = EstadoSuperficie(
                    superficie=str(self.data['estado_super'][index])
                )
            estado_superficie.save()
        print(self.data['estado_super'])

    def save_terreno(self):
        for index in self.data.index:
            try:
                Terreno.objects.get(terreno=self.data['terreno'][index])
            except Terreno.DoesNotExist:
                terreno = Terreno(
                    terreno=str(self.data['terreno'][index])
                )
            terreno.save()
        print(self.data['terreno'])

    def save_fecha_acc(self):
        for index in self.data.index:
            try:
                FechaAcc.objects.get(pk=index)
            except FechaAcc.DoesNotExist:
                fecha_acc = FechaAcc(
                    fecha=datetime(
                        int(self.data['Año'][index]),
                        int(convert_month(str(self.data['Mes'][index]))),
                        int(self.data['Día'][index]),
                    ).date(),
                    hora=str(self.data['hora_acc'][index])
                )
            fecha_acc.save()
        print(f"{self.data['Año']}/{self.data['Mes']}/{self.data['Día']}")

    def save_dia_accidente(self):
        indexs = FechaAcc.objects.all().count()
        for i in range(1, indexs):
            rand_dia = np.random.randint(1, DiaSemanaAcc.objects.all().count())
            rand_condicion = np.random.randint(1, CondicionMeteor.objects.all().count())
            rand_estado = np.random.randint(1, EstadoSuperficie.objects.all().count())
            rand_terreno = np.random.randint(1, Terreno.objects.all().count())
            try:
                DiaAccidente.objects.get(pk=i)
            except DiaAccidente.DoesNotExist:
                dia_accidente = DiaAccidente(
                    dia_semana_acc=DiaSemanaAcc.objects.get(pk=rand_dia),
                    condicion_meteor=CondicionMeteor.objects.get(pk=rand_condicion),
                    estado_superficie=EstadoSuperficie.objects.get(pk=rand_estado),
                    terreno=Terreno.objects.get(pk=rand_terreno),
                    fecha_acc=FechaAcc.objects.get(pk=i),
                )
            dia_accidente.save()
        print('done')

    def save_departamento(self):
        for index in self.data.index:
            try:
                Departamento.objects.get(territorial=self.data['territorial'][index])
            except Departamento.DoesNotExist:
                departamento = Departamento(
                    territorial=str(self.data['territorial'][index])
                )
            departamento.save()
        print(self.data['territorial'])

    def save_via(self):
        for index in self.data.index:
            rand_territorial = np.random.randint(1, Departamento.objects.all().count())
            try:
                Via.objects.get(codigo_via=self.data['codigo_via'][index])
            except Via.DoesNotExist:
                via = Via(
                    codigo_via=str(self.data['codigo_via'][index]),
                    estado_super=str(self.data['estado_super'][index]),
                    terreno=str(self.data['terreno'][index]),
                    geometria_acc=str(self.data['geometria_acc'][index]),
                    id_departamento=Departamento.objects.get(pk=int(rand_territorial)),
                )
            via.save()
        print(self.data['codigo_via'])

    def save_accidente(self):
        for index in self.data.index:
            fecha_registro_date = self.data['fecha_registro'][index].split(' ')[0].split('-')
            fecha_accidente_date = self.data['fecha_acc'][index].split(' ')[0].split('-')
            rand_via = np.random.randint(1, Via.objects.all().count())
            try:
                Accidente.objects.get(pk=index)
            except Accidente.DoesNotExist:
                accidente = Accidente(
                    fecha_registro=datetime(
                        int(fecha_registro_date[0]),
                        int(fecha_registro_date[1]),
                        int(fecha_registro_date[2]),
                    ).date(),
                    fecha_accidente=datetime(
                        int(fecha_accidente_date[0]),
                        int(fecha_accidente_date[1]),
                        int(fecha_accidente_date[2]),
                    ).date(),
                    dia_semana_acc=str(self.data['dia_semana_acc'][index]),
                    n_heridos=int(self.data['n_heridos'][index]),
                    n_muertos=int(self.data['n_muertos'][index]),
                    clase_accidente=str(self.data['clase_accidente'][index]),
                    id_via=Via.objects.get(codigo_via=str(self.data['codigo_via'][index])),
                )
            accidente.save()
        print(self.data[['fecha_registro', 'fecha_acc']])
