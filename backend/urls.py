from django.urls import path, include
from django.contrib import admin
#from api.urls import router
from rest_framework_simplejwt import views as jwt_views


app_name = 'init'


urlpatterns = [
    path('home/', include('api.urls')),
    path('', include('django.contrib.auth.urls')),
    path('admin/', admin.site.urls),
    #path('api/', include(router.urls), name='api'),
    path('api/', include('api.urls')),
    path('api/token/', jwt_views.TokenObtainPairView.as_view(),
         name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(),
         name='token_refresh'),
]