from .models import *
from django.shortcuts import render
import pandas as pd
from utils.save_data import SaveData


def home(request):
    #data = pd.read_csv('excel/datalimpia.csv')
    data = pd.read_excel('excel/data_mart_2.xlsx')
    save_data = SaveData(data)

    ######### DataMart 1 #########

    #### save CondicionMeteor
    #save_data.save_condicion_meteor()

    #### save DiaSemanaAcc
    #save_data.save_dia_semana_acc()

    #### save EstadoSuperficie
    #save_data.save_estado_superficie()

    #### save Terreno
    #save_data.save_terreno()

    #### save FechaAcc
    #save_data.save_fecha_acc()

    #### save DiaAccidente
    #save_data.save_dia_accidente()

    ######### DataMart 2 #########

    #### save Departamento
    #save_data.save_departamento()

    #### save Via
    #save_data.save_via()

    #### save Accidente
    save_data.save_accidente()

    return render(request, 'hello.html')