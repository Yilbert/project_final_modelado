from django.contrib import admin
from .models import User, CondicionMeteor, DiaSemanaAcc, EstadoSuperficie, Terreno, FechaAcc, DiaAccidente
from .models import Departamento, Via, Accidente


admin.site.register(User)
admin.site.register(CondicionMeteor)
admin.site.register(DiaSemanaAcc)
admin.site.register(EstadoSuperficie)
admin.site.register(Terreno)
admin.site.register(FechaAcc)
admin.site.register(DiaAccidente)
admin.site.register(Departamento)
admin.site.register(Via)
admin.site.register(Accidente)