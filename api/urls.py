from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import home


router = DefaultRouter()


urlpatterns = [
    path('', home, name='home'),
] + router.urls

