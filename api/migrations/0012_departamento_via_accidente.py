# Generated by Django 4.0.4 on 2022-07-22 14:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0011_remove_accidente_id_departamento_delete_via_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='Departamento',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('territorial', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Via',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigo_via', models.IntegerField()),
                ('estado_super', models.CharField(max_length=40)),
                ('terreno', models.CharField(max_length=40)),
                ('geometria_acc', models.CharField(max_length=40)),
                ('id_departamento', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.departamento')),
            ],
        ),
        migrations.CreateModel(
            name='Accidente',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_registro', models.DateField()),
                ('fecha_accidente', models.DateField()),
                ('dia_semana_acc', models.CharField(max_length=20)),
                ('n_heridos', models.IntegerField()),
                ('n_muertos', models.IntegerField()),
                ('clase_accidente', models.CharField(max_length=20)),
                ('id_via', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.via')),
            ],
        ),
    ]
