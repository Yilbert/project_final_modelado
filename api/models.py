from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    pass


class CondicionMeteor(models.Model):
    condicion = models.CharField(max_length=20)

    def __str__(self):
        return self.condicion


class DiaSemanaAcc(models.Model):
    dia_semana = models.CharField(max_length=20)

    def __str__(self):
        return self.dia_semana


class EstadoSuperficie(models.Model):
    superficie = models.CharField(max_length=40)

    def __str__(self):
        return self.superficie


class Terreno(models.Model):
    terreno = models.CharField(max_length=20)

    def __str__(self):
        return self.terreno


class FechaAcc(models.Model):
    fecha = models.DateField()
    hora = models.PositiveIntegerField()

    def __str__(self):
        return self.fecha.strftime('%Y-%m-%d') + ' ' + str(self.hora)


class DiaAccidente(models.Model):
    dia_semana_acc = models.ForeignKey(DiaSemanaAcc, on_delete=models.CASCADE)
    condicion_meteor = models.ForeignKey(CondicionMeteor, on_delete=models.CASCADE)
    estado_superficie = models.ForeignKey(EstadoSuperficie, on_delete=models.CASCADE)
    terreno = models.ForeignKey(Terreno, on_delete=models.CASCADE)
    fecha_acc = models.ForeignKey(FechaAcc, on_delete=models.CASCADE)


class Departamento(models.Model):
    territorial = models.CharField(max_length=100)

    def __str__(self):
        return self.territorial


class Via (models.Model):
    codigo_via = models.CharField(max_length=10)
    estado_super = models.CharField(max_length=50)
    terreno = models.CharField(max_length=50)
    geometria_acc = models.CharField(max_length=50)
    id_departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.codigo_via)


class Accidente(models.Model):
    fecha_registro = models.DateField()
    fecha_accidente = models.DateField()
    dia_semana_acc = models.CharField(max_length=50)
    n_heridos = models.IntegerField()
    n_muertos = models.IntegerField()
    clase_accidente = models.CharField(max_length=50)
    id_via = models.ForeignKey(Via, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.fecha_accidente) + ' ' + str(self.id_via)